class String
	def parse_json
		JSON.parse self, {symbolize_names: true}
	end
end
