module SkillCommand
	def attack attacker, defender
		defender.attacked attacker.attack
	end
end
