class Board
def initialize x, y
@board = Array.new y { Array.new x }
@characters = {}
end

def set position, character
@board[position.x][position.y] = character
@characters[character.id] = position
end

def position character
@chracters[character.id]
end

def character position
@board[position.x][position.y]
end

def characters arrow
arrow.map &method(:character)
end

class Position
attr_reader :x, :y

def initialize x, y
@x = x
@y = y
end

def + other
@x += other.x
@y += other.y
self
end

def up
@y += 1
self
end

def down
@y -= 1
self
end

def left
@x -= 1
self
end

def right
@x += 1
self
end

def up_left
up.left
end

def up_right
up.right
end

def down_left
down.left
end

def down_right
down.right
end
