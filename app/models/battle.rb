class Battle < ApplicationRecord
  belongs_to :battle_id
  belongs_to :operator_id
  belongs_to :deck_id
end
