class CreateUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |t|
      t.references :operator_id, foreign_key: true, null:false
      t.string :name, null:false, default:""
      t.string :password, null:false, default:""

      t.timestamps
    end
  end
end
