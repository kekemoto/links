class CreateCharacters < ActiveRecord::Migration[5.1]
	def change
		create_table :characters do |t|
			t.string :name, null: false, default: ""
			t.integer :hp, null: false
			t.integer :atk, null: false
			t.references  :skills, foreign_key: true, null: false, default: "[]"

			t.timestamps
		end
	end
end
