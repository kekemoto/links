class CreateBattles < ActiveRecord::Migration[5.1]
  def change
    create_table :battles do |t|
      t.references :battle_id, foreign_key: true, null: false
      t.references :operator_id, foreign_key: true, null: false
      t.references :deck_id, foreign_key: true, null: false

      t.timestamps
    end
  end
end
