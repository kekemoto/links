class CreateCpus < ActiveRecord::Migration[5.1]
  def change
    create_table :cpus do |t|
      t.references :operator_id, foreign_key: true, null:false
      t.string :name, null:false, default:""
      t.integer :ai_type, null:false, default:1

      t.timestamps
    end
  end
end
