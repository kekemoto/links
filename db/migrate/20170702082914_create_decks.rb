class CreateDecks < ActiveRecord::Migration[5.1]
  def change
    create_table :decks do |t|
      t.string :name, null: false, default: ""
      t.string :characters, null: false, default: "[]"
      t.references :operator_id, foreign_key: true, null: false

      t.timestamps
    end
  end
end
